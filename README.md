Ontologies for Irradiation Experiment Data Management  
=
  
CERN, Mines Paris (PSL University) 
  
IEDM
-

IEDM is an OWL-based Irradiation Experiment Data Management ontology.

The description of an actual irradiation experiment recently performed at IRRAD, the CERN proton irradiation facility, is provided. 

One of the key design choices for IEDM was to maximize the reuse of existing foundational ontologies such as the Ontology of Scientific Experiments (EXPO), the Ontology of Units of Measure (OM) and the Friend-of-a-Friend Ontology (FOAF).

See the following paper: Gkotse, B., Jouvelot, P., & Ravotti, F. (2019). _IEDM: An Ontology for Irradiation Experiments Data Management_. In The Semantic Web: ESWC 2019 Satellite Events: ESWC 2019 Satellite Events, Portorož, Slovenia, June 2–6, 2019, Revised Selected Papers 16 (pp. 80-83). Springer International Publishing.

ORB
-

ORB is an OWL-based ontology for the Open Review Base dataset.

Keywords: Ontology, OWL, irradiation experiment, data management, High Energy Physics.
